import network
import micropython
import uasyncio
from time import sleep, time
from machine import Pin, reset_cause, freq, TouchPad
from app import app

freq(240000000)

PREVENT_BOOT_PIN = 0
touchable_pin = TouchPad(Pin(PREVENT_BOOT_PIN))
if touchable_pin.read() > 100: #100 is set by feeling it is used to prevent normal boot
    micropython.alloc_emergency_exception_buf(16)
    uasyncio.run(app())
else:
    print("init failed FLASH is high")
