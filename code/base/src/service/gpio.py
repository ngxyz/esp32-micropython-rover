import machine
#List of hardware functions
I2C = "I2C"
WHEEL_CONTACT_FL = "WHEEL_CONTACT_FL"
WHEEL_CONTACT_FR = "WHEEL_CONTACT_FR"
WHEEL_CONTACT_RL = "WHEEL_CONTACT_RL"
WHEEL_CONTACT_RR = "WHEEL_CONTACT_RR"
LED= "LED"
#bps constants
BITRATE_100K = 100000
BITRATE_400K = 400000
function_to_gpio_map = {
    I2C:{
        "scl": 21,
        "sda": 22,
        },
    WHEEL_CONTACT_FL: -1,
    WHEEL_CONTACT_FR: -1,
    WHEEL_CONTACT_RL: -1,
    WHEEL_CONTACT_RR: -1,
    LED: 5
}
gpio_operation_mode = {
    LED: machine.Pin.OUT
}
allocated_gpio = {}

def _allocate_pin(module_name):
    return machine.Pin(function_to_gpio_map[module_name], gpio_operation_mode[module_name])

def _allocate_i2c_bus(module_name):
    serial_clock = function_to_gpio_map[module_name]["scl"]
    serial_data = function_to_gpio_map[module_name]["sda"]
    return machine.SoftI2C(scl = machine.Pin(serial_clock),
                       sda = machine.Pin(serial_data),
                       freq=BITRATE_100K)

def allocate_gpio_for(module_name):
    gpio_init_functions = {
     LED: _allocate_pin,
     I2C: _allocate_i2c_bus
    }
    if module_name not in allocated_gpio:
        gpio = gpio_init_functions[module_name](module_name)
        allocated_gpio[module_name] = gpio
        return gpio
    else:
        return allocated_gpio[module_name]
