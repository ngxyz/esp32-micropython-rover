import gc
import esp

#GC profiles constants
AGGRESIVE_GC = 'AGGRESIVE'
DEFAULT_GC = 'DEFAULT_GC'
ON_OOM_GC = 'ON_OOM_GC'
AFTER_ALLOC_GC = 'AFTER_ALLOC_GC'
NO_GC = 'NO_GC'

class Memory:
    def memory_info():
        heap_allocated = gc.mem_alloc()
        heap_free = gc.mem_free()
        flash_size = esp.flash_size()
        return {
            "heap":{
                "allocated": heap_allocated,
                "free": heap_free,
                "total" : heap_allocated + heap_free
                },
            "flash":{
                "allocated": -1,
                "free": -1,
                "total" : flash_size
                }
            }

    def adjust_gc(gc_profile = None):
        return {
            AGGRESIVE_GC: lambda: gc.threshold(int(memory_info()["heap"]["total"]/64)),
            DEFAULT_GC: lambda: gc.threshold(int(memory_info()["heap"]["total"]/4)),
            ON_OOM_GC: lambda: gc.threshold(-1),
            AFTER_ALLOC_GC: lambda: gc.threshold(16),
            NO_GC: lambda: gc.disable(),
            None: lambda: gc.collect()
            }[gc_profile]()
