from src.service.gpio import allocate_gpio_for, I2C
from src.domain.servo import Servo
from time import sleep
from machine import Timer
import math
import uasyncio

#Motor Location
FL = 15 #front left
FR = 14
RL = 13 #rear left
RR = 12
#All Steering motors
STEERING_MOTORS = [FL, FR, RL, RR]
#Propulsion motor directions
FORWARD = 1
BACKWARD = 0
#list of timings
FULL_RANGE_DURATION = 450 # amount of time[ms] needed for 180deg of travel 4.8V
PER_DEGREE_DURATION = FULL_RANGE_DURATION / 180 # amount of time[ms] needed for 1deg of travel 4.8V
#list of positions
START = 1
MIDDLE = 90
END = 180

class Mobility:
    def __init__(self):
        self.i2c = allocate_gpio_for(I2C)
        self.servo = Servo(self.i2c)
        self.servo_positions = {
            FL:MIDDLE,
            FR:MIDDLE,
            RL:MIDDLE,
            RR:MIDDLE
        }

    def lock_position(self):
        for servo in STEERING_MOTORS:
            self.servo.position(servo, self.servo_positions[servo])

    def unlock_position(self):
        self.servo.release_many(STEERING_MOTORS)
        
    def steer(self, degrees, invert=False):
        servos = []
        if invert:
            servos.extend([RL, RR])
        else:
            servos.extend([FL, FR])
        for servo in servos:
            uasyncio.create_task(self.position_and_release(servo, degrees))

    async def move(self, direction, value):
        pass

    async def rotate(self, degrees):
        pass

    async def translate(self, degrees, value):
        pass

    async def sweep(self,servo_index):
        while True:
            for degrees in range(START, END+1):
                await self.position_and_release(servo_index, degrees)

    async def middle_position(self):
        for servo in STEERING_MOTORS:
            self.servo.position(servo, MIDDLE)
            self.servo_positions[servo_index] = MIDDLE 
        await uasyncio.sleep_ms(int(FULL_RANGE_DURATION / 2))
        self.servo.release_many(servos)

    def _calculate_travel_time(self, current_degree, next_degree):
        return math.ceil(math.fabs(next_degree - current_degree ) * PER_DEGREE_DURATION)

    async def position_and_release(self, servo_index, degree):
        self.servo.position(servo_index, degree)
        travel_time = self._calculate_travel_time(self.servo_positions[servo_index], degree)
        self.servo_positions[servo_index] = degree
        await uasyncio.sleep_ms(travel_time)
        self.servo.release(servo_index)
