import machine
#Voltage levels
V3 = "V3"
V5 = "V5"
#Power sources
SOLAR = "SOLAR"
BATTERY0 = "BATTERY0"

def battery_level():
    return {
        BATTERY0: {
                 "voltage": -1,
                 "precentage" : -1,
             }
        }

def power_consumption():
    return {
         V3: {
                 "current": -1,
                 "voltage": -1,
                 "power" : -1,
             },
         V5: {
                 "current": -1,
                 "voltage": -1,
                 "power" : -1,
             },
        }

def power_produciton():
    return {
        SOLAR: {
                 "current": -1,
                 "voltage": -1,
                 "power" : -1,
             }
        }

def power_info():
    return {
        "consumption": power_consumption(),
        "production": power_produciton(),
        "battery": battery_level(),
        }

def light_sleep_for(seconds = 10):
    machine.lightsleep(seconds * 1000)

def deep_sleep_for(sec):
  #configure RTC.ALARM0 to be able to wake the device
  rtc = machine.RTC()
  rtc.irq(trigger=rtc.ALARM0, wake=machine.DEEPSLEEP)
  # set RTC.ALARM0 to fire after Xmilliseconds, waking the device
  rtc.alarm(rtc.ALARM0, sec * 1000)
  #put the device to sleep
  machine.deepsleep()

def enter_idle_state():
    machine.idle()

def light_sleep_after_callback(cb, seconds = 10):
    cb()
    light_sleep_for(seconds)

def deep_sleep_after_callback(cb, seconds = 10):
     cb()
     deep_sleep_for(seconds)

def idle_after_callback(cb):
    cb()
    enter_idle_state()

