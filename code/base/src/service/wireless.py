import network
import uasyncio
import time

class Wireless:
    def __init__(self):
        self.client = network.WLAN(network.STA_IF)
        self.access_point = network.WLAN(network.AP_IF)
        
    async def connect_to_access_point(self, ssid, password, timeout = 5):
        if not self.client.isconnected():
            connection_initiated = time.time()
            self.client.active(True)
            self.client.connect(ssid, password)
            while not self.client.isconnected():
                if(time.time() - connection_initiated > timeout):
                    self.client.active(False)
                    raise Exception(
                        
                        "Connect to AP timeout")
                await uasyncio.sleep_ms(300)
        return self.client
    
    async def make_access_point(self, ssid, password):
        self.access_point.config(essid=ssid, password=password)
        self.access_point.active(True)
    
    async def disconnect_from_access_point(self):
        self.client.disconnect()
        self.client.active(False)
    
    def destroy_access_point(self):
        pass
    
        
    
    