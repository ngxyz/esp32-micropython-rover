const { exec } = require("child_process")
const path =  require("path")
const fs = require("fs")

const MY_NAME = path.basename(process.argv[1])
const WORK_DIR = process.argv[3] || path.dirname(process.argv[1])
const COMMAND = process.argv[2]

console.log(66666666666666)
console.log(path.basename(process.argv[1]))
console.log(process.argv[3])
console.log(path.dirname(process.argv[1]))
console.log(66666666666666)
const ampyCallRateLimit = 10000
let ampyCallTimestamp = new Date().getTime()
const waitForSerialToCoolDown = () => {
    while(new Date().getTime() - ampyCallTimestamp < ampyCallRateLimit){}
        ampyCallTimestamp = new Date().getTime()
}

const downloadRemoteFile = (remotePath, localPath) => {
    console.log("downloading >>> ", remotePath, " to ", localPath)
    if (!fs.existsSync(localPath)) fs.mkdirSync(path.parse(localPath).dir, { recursive: true })
    exec(`ampy get ${remotePath} ${localPath}`, (error, stdout) => {
        if(error)
            console.log(error)
    })
}
const createRemoteDir = (remotePath) => {
    console.log("creating dir >>> ", remotePath)
    exec(`ampy mkdir ${remotePath}`, (error, stdout) => {
        if(error)
            console.log(error)
    })
}
const uploadLocalFile = (localPath, remotePath) => {
    console.log("uploading >>> ", localPath, " to ", remotePath)
    exec(`ampy put ${localPath} ${remotePath}`, (error, stdout) => {
        if(error)
            console.log(error)
    })
}
const exploreLocalDirTree = (filePath) => {
    filePath = filePath || WORK_DIR
    console.log(filePath)
    fs.readdir(filePath , function (err, files) {
    if (err) {
        console.log('Unable to scan directory: ' + err);
        process.exit(1)
    } 
    files.forEach(function (file) {
        // -----BEGIN NASTY HACK-----
        waitForSerialToCoolDown()
        // -----END NASTY HACK-----
        const parsed = path.parse(file)
        if(parsed.ext !==""){
            if(parsed.base !== MY_NAME){
                uploadLocalFile(filePath + path.sep + file,(filePath + path.sep + file).substring(WORK_DIR.length))
            }
        }
        else {
            createRemoteDir((filePath + path.sep + file).substring(WORK_DIR.length))
            exploreLocalDirTree(filePath + path.sep + file)
        }
    });
});
}
const exploreRemoteDirTree = (filePath) => {
    // depth first exploration
    exec(`ampy ls ${filePath ? filePath : " "}`, (error, stdout) => {
    if (error) {
        console.error(`Make sure that ESP is connected and visible`);
        return;
    }
    const filesAtPath = stdout.split("\n").filter(Boolean)
    filesAtPath.forEach(file => {
        // -----BEGIN NASTY HACK-----
        waitForSerialToCoolDown()
        // -----END NASTY HACK-----
        const parsed = path.parse(file)
        if(parsed.ext !=="")
            downloadRemoteFile(file,WORK_DIR+file)
        else
            exploreRemoteDirTree(file)
    })
});
}

const commands = {
    "upload": exploreLocalDirTree,
    "download": exploreRemoteDirTree,
    "a": ()=>{""}
}

const command = commands[COMMAND]

if(!command){
    if (COMMAND){
        console.error(`Command "${COMMAND}" is not supported, try "upload" or "download" instead.`)
        process.exit(1)
    }else{
        console.error(`Command not provided, try "upload" or "download" `)
        process.exit(1)
    }
}

command()
